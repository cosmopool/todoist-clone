# Todoist Clone
Self-hosted multiplatform Todoist clone (in the making).

### :iphone: Frontend Roadmap
- :white_medium_square: Basic interface
- :white_medium_square: Inbox
- :white_medium_square: Today
- :white_medium_square: Upcoming
- :white_medium_square: Tasks
- :white_medium_square: Projects
- :white_medium_square: Due dates
- :white_medium_square: Recurring due dates
- :white_medium_square: Comments
- :white_medium_square: Filters
- :white_medium_square: Icons support
- :white_medium_square: Drag and Drop
- :white_medium_square: Progress indication
- :white_medium_square: Offline support
- :white_medium_square: Language parser
- :white_medium_square: Views
- :white_medium_square: Themes
- :white_medium_square: Quick find
- :white_medium_square: Karma
- :question: End-to-end encryption

### :cloud: Backend Roadmap

- :white_medium_square: Database
- :white_medium_square: Api design
- :white_medium_square: Tasks
- :white_medium_square: Projects
- :white_medium_square: Comments
- :white_medium_square: Due dates
- :white_medium_square: Filters
- :white_medium_square: Icons
- :white_medium_square: Todoist synchronization
- :white_medium_square: Views
- :white_medium_square: Docker image
- :white_medium_square: Karma
- :question: End-to-end encryption
